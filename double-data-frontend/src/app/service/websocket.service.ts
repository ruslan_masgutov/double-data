import {Injectable} from "@angular/core";

let SockJS = require("sockjs-client");
let StompWrapper = require("stompjs/lib/stomp.js");

@Injectable()
export class WebSocketService {
    private stompClient: any;

    connect(onMessage?: (message: any)=>void,): void {
        let defaultOnMessage = message => {
            console.log('Message: ' + message);
            console.log('Message body: ' + message.body);
        };
        onMessage = onMessage || defaultOnMessage;

        let socket = new SockJS('/websocket-point');
        this.stompClient = StompWrapper.Stomp.over(socket);
        this.stompClient.connect({}, frame => {
            console.log('Connected: ' + frame);
            this.stompClient.subscribe(`/topic/${localStorage.getItem('user-token')}`, onMessage);
        })
    }

    disconnect(): void {
        this.stompClient.disconnect();
        console.log("Disconnected");
    }

}