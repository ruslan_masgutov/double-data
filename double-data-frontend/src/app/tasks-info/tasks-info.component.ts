import {Component, OnInit, ViewChild, OnDestroy} from "@angular/core";
import {Http, RequestOptionsArgs, Headers} from "@angular/http";
import {Page} from "../dto/page";
import {ModalDirective} from "ng2-bootstrap";
import {WebSocketService} from "../service/websocket.service";
// import *  from "moment";
import moment = require("moment");
// let moment = require('moment/min/moment.min.js');

@Component({
    selector: 'dd-tasks-info',
    template: require('./tasks-info.html').toString(),
    styles: [require('./tasks-info.css').toString()]
})
export class TasksInfoComponent implements OnInit, OnDestroy {
    @ViewChild('lgModal') public modal: ModalDirective;
    options: RequestOptionsArgs;
    tasks: any[] = [];
    page: Page = new Page();
    selectedTask: any;
    src: string;
    algorithm: string;
    timers: any = {};
    defaultErrorCallback = (error) => {
        alert(error.text());
        console.log(error.text());
    };

    onMessage = (message) => {
        let data = JSON.parse(message.body);
        data.duration = this.getDurationTask(data);
        let ind = this.tasks.findIndex((value) => value.id == data.id);
        if (ind >= 0) {
            this.tasks[ind] = data;
        }
        this.dropTimer(data);
    };

    createTimer(task: any) {
        if (task.status == 'IN_PROCESS') {
            this.timers[task.id] = setInterval(() => {
                task.duration += 1
            }, 1000)
        }
    }

    dropTimer(task: any) {
        if (!(task.status == 'IN_PROCESS')) {
            if (this.timers[task.id]) {
                clearInterval(this.timers[task.id])
            }
        }
    }

    createTask() {
        this.http.post(`/api/tasks?src=${this.src}&algo=${this.algorithm}`, {}, this.options)
            .subscribe(() => {
                this.getTasks()
            }, this.defaultErrorCallback);
        this.modal.hide()
    }

    deleteTask(task: any) {
        this.http.delete(`/api/tasks/${task.id}`, this.options)
            .subscribe(() => {
                this.getTasks()
            }, this.defaultErrorCallback);
    }

    cancelTask(task: any) {
        this.http.put(`/api/tasks/${task.id}/cancel`, {}, this.options)
            .subscribe(() => {
                this.getTasks()
            }, this.defaultErrorCallback);
    }


    canDelete(task: any): boolean {
        return task && task.status != 'IN_PROCESS' && task.status != 'WAIT'
    }

    canCancel(task: any): boolean {
        return task && task.status != 'SUCCESSFUL' && task.status != 'FAIL'
    }

    getDurationTask(task: any) {
        if (task && task.createdAt && task.finishedAt) {
            return moment(task.finishedAt).diff(moment(task.createdAt), 'seconds');
        }
        if (task && task.createdAt) {
            let time = moment().diff(moment(task.createdAt), 'seconds');
            return time >= 0 ? time : 0;
        }
        return null;
    }

    pageChanged(event: any) {
        this.page.page = event.page;
        this.getTasks();
    }

    selectTask(task) {
        this.selectedTask = task
    }

    ngOnInit(): void {
        this.websocketService.connect(this.onMessage)
    }

    ngOnDestroy(): void {
        this.websocketService.disconnect()
    }

    private getTasks() {
        this.http.get(`/api/tasks?page=${this.page.page - 1}`, this.options).subscribe(response => {
            let data = response.json();
            this.tasks = data.content;
            this.tasks.forEach((task) => {
                task.duration = this.getDurationTask(task);
                this.createTimer(task);
            });
            this.page.total = data.total;
            this.selectedTask = this.tasks[0]
        }, this.defaultErrorCallback)
    }

    constructor(private http: Http, private websocketService: WebSocketService) {
        let header = new Headers();
        header.set('Authorization', localStorage.getItem('user-token'));
        this.options = {
            headers: header
        };

        this.getTasks();
    }
}