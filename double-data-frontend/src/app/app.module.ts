import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {TasksInfoComponent} from "./tasks-info/tasks-info.component";
import {PaginationModule, ModalModule} from "ng2-bootstrap";
import {FormsModule} from "@angular/forms";
import {WebSocketService} from "./service/websocket.service";

@NgModule({
    imports: [BrowserModule, HttpModule, FormsModule, PaginationModule, ModalModule],
    declarations: [AppComponent, TasksInfoComponent],
    bootstrap: [AppComponent],
    providers: [WebSocketService]
})
export class AppModule {
}