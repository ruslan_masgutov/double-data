import {Component, OnInit} from "@angular/core";
import {Http} from "@angular/http";

@Component({
    selector: 'dd-app',
    template: require('./app.html').toString(),
    styles: [require('./app.css').toString()]
})
export class AppComponent implements OnInit {
    userToken: string;

    ngOnInit(): void {
        let token = localStorage.getItem('user-token');
        if (token) {
            this.userToken = token
        } else {
            this.http.get('/api/users')
                .subscribe(response => {
                    let token = response.headers.get('authorization') || response.headers.get('Authorization');
                    localStorage.setItem('user-token', token);
                    this.userToken = token;
                })
        }
    }

    constructor(private http: Http) {
    }
}
