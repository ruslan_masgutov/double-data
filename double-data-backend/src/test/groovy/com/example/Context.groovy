package com.example

import com.example.model.User
import com.example.repository.TaskRepository
import com.example.repository.UserRepository
import com.example.service.CurrentUserService
import com.example.service.TaskService
import com.example.utils.DatabaseConfigurationTest
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.TestRestTemplate
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.RequestEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import java.util.stream.Collectors

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(
    classes = [
        DatabaseConfigurationTest,
        DoubleDataBackendApplication,
    ])
@Transactional(propagation = Propagation.NEVER)
class Context extends Specification {
    private static final long TEN_DAYS = 1000 * 60 * 60 * 24 * 10;

    @LocalServerPort
    int port

    @Autowired
    UserRepository userRepository
    @Autowired
    CurrentUserService currentUserService
    @Autowired
    TaskService taskService
    @Autowired
    TaskRepository taskRepository

    User currentUser

    RestTemplate restTemplate = new TestRestTemplate()

    URI serviceURI(String path = "", String params = null) {
        def url = "http://localhost:$port/${path}"
        if (StringUtils.isNotBlank(params)) {
            url += "?$params"
        }
        new URI(url)
    }

    void setup() {
        User userSender = userRepository.findOneByToken(UUID.fromString('dfac5f7e-959c-4757-972f-0cc4e7a8e6c3')).get()
        currentUserService.currentUser = userSender
        this.currentUser = userSender
    }

    void cleanup() {
        taskService.currentTasks.entrySet().stream()
            .forEach({ entry -> entry.value.get() })
    }

    RequestEntity get(String path, Map<String, Object> params = [:]) {
        def paramString = params.keySet().stream().map({ key -> "$key=${params.get(key)}" }).collect(Collectors.joining("&"))
        RequestEntity.get(serviceURI(path, paramString))
            .header("Authorization", currentUserService.currentUser.token.toString())
            .build()
    }

    RequestEntity post(String path, def body = null) {
        RequestEntity.post(serviceURI(path))
            .header("Authorization", currentUserService.currentUser.token.toString())
            .body(body)
    }

    RequestEntity delete(String path) {
        RequestEntity.delete(serviceURI(path, null))
            .header("Authorization", currentUserService.currentUser.token.toString())
            .build()
    }

    RequestEntity put(String path, def body = null) {
        RequestEntity.put(serviceURI(path, null))
            .header("Authorization", currentUserService.currentUser.token.toString())
            .body(body)
    }

}
