package com.example.service

import com.example.exception.NotFoundException
import com.example.model.Algorithm
import com.example.model.Task
import com.example.model.TaskStatus
import com.example.model.User
import com.example.repository.TaskRepository
import spock.lang.Specification
import spock.lang.Unroll

import java.util.concurrent.CompletableFuture

import static com.example.utils.TestUtils.longUid

class TaskServiceImplTest extends Specification {
    def subj = new TaskServiceImpl()
    TaskService taskService = Mock(TaskService)
    TaskRepository taskRepository = Mock(TaskRepository)
    CurrentUserService currentUserService = Mock(CurrentUserService)

    def setup() {
        subj.taskService = taskService
        subj.taskRepository = taskRepository
        subj.currentUserService = currentUserService
    }

    def "createTask() Задача должна сохраниться в базу и начаться обработка"() {
        given:
        def src = "someSrc$longUid".toString()
        Algorithm algorithm = Algorithm.MD5
        Task expectedTask = [src: src, algorithm: algorithm, status: TaskStatus.WAIT]
        when:
        Task actualTask = subj.createTask(src, algorithm)

        then:
        actualTask == expectedTask
        1 * taskRepository.save(_) >> expectedTask
        1 * taskService.startTask(expectedTask)
    }

    @Unroll
    def "deleteTask() Задача удаляется, если она находится в финальном статусе(#status)"(TaskStatus status) {
        given:
        Task task = createTask()
        task.id = longUid
        task.status = status

        when:
        subj.deleteTask(task.id)

        then:
        1 * taskRepository.findOneById(task.id) >> Optional.of(task)
        1 * taskRepository.delete(task.id)

        where:
        status                | _
        TaskStatus.FAIL       | _
        TaskStatus.SUCCESSFUL | _
    }

    @Unroll
    def "deleteTask() Невозможно удалить задачу, если она находится не в финальном статусе(#status)"(TaskStatus status) {
        given:
        Task task = createTask()
        task.id = longUid
        task.status = status

        when:
        subj.deleteTask(task.id)

        then:
        thrown(NotFoundException)
        1 * taskRepository.findOneById(task.id) >> Optional.empty()
        0 * taskRepository.delete(task.id)

        where:
        status                | _
        TaskStatus.WAIT       | _
        TaskStatus.IN_PROCESS | _
    }

    @Unroll
    def "cancelTask() Задача отменяется, если она находится не в финальном статусе(#status)"(TaskStatus status) {
        given:
        Task task = createTask()
        task.id = longUid
        task.status = status

        when:
        Task task1 = subj.cancelTask(task.id)

        then:
        task1.canceled
        1 * taskRepository.findOneById(task.id) >> Optional.of(task)
        2 * taskService.currentTasks >> [:]
        1 * taskRepository.save(task) >> {
            task.canceled = true
            task
        }

        where:
        status                | _
        TaskStatus.WAIT       | _
        TaskStatus.IN_PROCESS | _
    }

    @Unroll
    def "cancelTask() Невозможно отменить задачу, если она находится в финальном статусе(#status)"(TaskStatus status) {
        given:
        Task task = createTask()
        task.id = longUid
        task.status = status

        when:
        subj.deleteTask(task.id)

        then:
        thrown(NotFoundException)
        1 * taskRepository.findOneById(task.id) >> Optional.empty()
        0 * taskService.currentTasks >> [:]
        0 * taskRepository.save(task)

        where:
        status                | _
        TaskStatus.FAIL       | _
        TaskStatus.SUCCESSFUL | _
    }

    def "startTask() запуск и обработка задачи"() {
        given:
        Task task = createTask()
        task.id = longUid

        when:
        subj.startTask(task)

        then:
        1 * taskService.processTask(task) >> CompletableFuture.completedFuture(task)
        1 * taskService.successful(task)
        2 * taskService.currentTasks >> [:]

    }

    private static createTask() {
        new Task(
            algorithm: Algorithm.MD5,
            owner: [id: longUid] as User,
            status: TaskStatus.WAIT,
            src: "someSrc$longUid"
        )
    }
}
