package com.example.utils

import com.example.model.User
import com.example.service.CurrentUserService
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service

@Profile("test")
@Service
class CurrentUserServiceTestImpl implements CurrentUserService {
    User currentUser


    @Override
    User getCurrentUser() {
        currentUser
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    void setCurrentUser(User user) {
        currentUser = user
    }


}
