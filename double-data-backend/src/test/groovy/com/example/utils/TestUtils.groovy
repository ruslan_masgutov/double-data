package com.example.utils

import java.util.concurrent.atomic.AtomicLong

public class TestUtils {
    private static final AtomicLong uid = new AtomicLong(0);

    public static Integer getUid() {
        return (int) uid.getAndIncrement();
    }

    public static Long getLongUid() {
        return uid.getAndIncrement();
    }

    public static BigDecimal generate() {
        return new BigDecimal(BigInteger.valueOf(new Random().nextInt(10000001)), 2);
    }
}
