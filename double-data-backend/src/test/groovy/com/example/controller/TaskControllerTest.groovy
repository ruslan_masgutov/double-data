package com.example.controller

import com.example.Context
import com.example.model.Algorithm
import com.example.model.Task
import com.example.utils.RestResponsePage
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import spock.lang.Unroll

class TaskControllerTest extends Context {
    @Unroll
    def "GET /api/tasks получение страницы(#pageNumber) с тасками"(int pageNumber, int expectedCount) {
        given:
        Page<Task> expectedPage = taskRepository.findAllByOwner(currentUser, new PageRequest(pageNumber, 10, Sort.Direction.ASC, 'id'))

        when:
        def response = restTemplate.exchange(get('/api/tasks', [page: pageNumber]), new ParameterizedTypeReference<RestResponsePage<Task>>() {
        })

        then:
        response.statusCode == HttpStatus.OK
        Page<Task> actualVacancyPage = response.body as PageImpl<Task>
        actualVacancyPage.content.size() == expectedCount
        (0..expectedCount - 1).forEach({ i ->
            assert expectedPage.content[i] == actualVacancyPage.content[i]
        })
        where:
        pageNumber | expectedCount
        0          | 10
        1          | 5
    }

    def "POST /api/tasks создание таски и отправка на исполнение"() {
        given:
        def src = 'http://cs5.pikabu.ru/post_img/2015/11/26/9/1448551521152298856.jpg'
        def algo = Algorithm.MD5.name()

        when:
        def response = restTemplate.exchange(post("/api/tasks?src=$src&algo=$algo"), Task)

        then:
        response.statusCode == HttpStatus.OK
        Task actualTask = response.body as Task
        taskRepository.findOne(actualTask.id) == actualTask

    }

    def "DELETE /api/tasks/{id} удаление законченной задачи"() {
        given:
        Long taskId = 1

        when:
        def response = restTemplate.exchange(delete("/api/tasks/$taskId"), Object)

        then:
        response.statusCode == HttpStatus.OK
        !taskRepository.findOneById(taskId).isPresent()

    }

    def "PUT /api/tasks/{id}/cancel отмена задачи, которая находится в ожидании или в процессе"() {
        given:
        Long taskId = 15

        when:
        def response = restTemplate.exchange(put("/api/tasks/$taskId/cancel"), Task)

        then:
        response.statusCode == HttpStatus.OK
        Task actualTask = response.body as Task
        taskRepository.findOne(actualTask.id).canceled

    }
}
