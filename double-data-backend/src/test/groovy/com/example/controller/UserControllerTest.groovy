package com.example.controller

import com.example.Context
import com.example.model.User
import org.spockframework.util.Assert
import org.springframework.http.HttpStatus
import org.springframework.http.RequestEntity

class UserControllerTest extends Context {
    def "GET /api/users должен вернуть информацию о текущем пользователе по заголовку Authorization"() {
        when:
        def response = restTemplate.exchange(get('/api/users'), User)

        then:
        response.statusCode == HttpStatus.OK
        User actualUser = response.body as User
        actualUser == currentUser
    }

    def "GET /api/users должен вернуть информацию о новом пользователе, если отсутствует Authorization"() {
        given:
        long initialCount = userRepository.count()

        when:
        def response = restTemplate.exchange(RequestEntity.get(serviceURI('/api/users')).build(), User)

        then:
        response.statusCode == HttpStatus.OK
        User actualUser = response.body as User
        Assert.notNull(actualUser.token)
        userRepository.count() == initialCount + 1


    }
}
