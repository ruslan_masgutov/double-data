CREATE TABLE IF NOT EXISTS public.users
(
  id           SERIAL NOT NULL,
  created_date TIMESTAMP WITH TIME ZONE DEFAULT now(),
  token        UUID,
  CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.tasks
(
  id          SERIAL              NOT NULL,
  algorithm   CHARACTER VARYING(255),
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT now(),
  error       CHARACTER VARYING,
  finished_at TIMESTAMP WITH TIME ZONE,
  result      CHARACTER VARYING,
  src         CHARACTER VARYING,
  started_at  TIMESTAMP WITH TIME ZONE,
  status      CHARACTER VARYING(255),
  owner_id    BIGINT,
  canceled    BOOL DEFAULT FALSE  NOT NULL,
  CONSTRAINT tasks_pkey PRIMARY KEY (id),
  CONSTRAINT fkh279lo9lqbhxqh68jq9sqs83s FOREIGN KEY (owner_id)
  REFERENCES users (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);
