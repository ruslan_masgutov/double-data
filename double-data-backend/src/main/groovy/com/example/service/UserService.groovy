package com.example.service

import com.example.model.User
import groovy.transform.CompileStatic

@CompileStatic
interface UserService {
    User login(String token)

    Optional<User> findByToken(String token)
}