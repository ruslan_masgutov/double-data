package com.example.service

import com.example.model.User
import com.example.util.UserAuthentication
import groovy.transform.CompileStatic
import org.springframework.context.annotation.Profile
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Profile("!test")
@CompileStatic
@Service
class CurrentUserServiceImpl implements CurrentUserService {
    @SuppressWarnings("GrMethodMayBeStatic")
    User getCurrentUser() {
        def details = SecurityContextHolder.context?.authentication?.details
        if (details instanceof User) {
            return details as User
        }
        null
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    void setCurrentUser(User user) {
        SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(user))
    }

}
