package com.example.service

import com.example.model.Algorithm
import com.example.model.Task
import com.example.model.TaskStatus
import groovy.transform.CompileStatic
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.annotation.Async

import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap

@CompileStatic
interface TaskService {
    ConcurrentHashMap<Long, CompletableFuture<Task>> getCurrentTasks()

    Task createTask(String src, Algorithm algo)

    void startTask(Task task)

    void deleteTask(Long id)

    Task cancelTask(Long id)

    @Async('delegatingSecurityExecutor')
    CompletableFuture<Task> processTask(Task task)

    void successful(Task task)

    void fail(Task task, Throwable throwable)

    Page<Task> findTaskByPage(Pageable pageable)

    Task changeStatus(Task task, TaskStatus status)
}