package com.example.service

import com.example.model.User
import groovy.transform.CompileStatic

@CompileStatic
interface CurrentUserService {
    User getCurrentUser()

    void setCurrentUser(User user)
}
