package com.example.service

import com.example.exception.NotFoundException
import com.example.model.Algorithm
import com.example.model.Task
import com.example.model.TaskStatus
import com.example.repository.TaskRepository
import groovy.util.logging.Slf4j
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.IOUtils
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.core.env.Environment
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.nio.file.Files
import java.nio.file.Paths
import java.security.NoSuchAlgorithmException
import java.time.LocalDateTime
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap
import java.util.function.BiConsumer

@Slf4j
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Service
class TaskServiceImpl implements TaskService {
    private ConcurrentHashMap<Long, CompletableFuture<Task>> tasks = new ConcurrentHashMap<>([:])
    @Autowired
    TaskService taskService
    @Autowired
    TaskRepository taskRepository
    @Autowired
    CurrentUserService currentUserService
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate

    @Override
    ConcurrentHashMap<Long, CompletableFuture<Task>> getCurrentTasks() {
        tasks
    }

    Task createTask(String src, Algorithm algo) {
        Task task = new Task(src: src, algorithm: algo, owner: currentUserService.currentUser)
        taskRepository.save(task)
        taskService.startTask(task)
        task
    }

    void deleteTask(Long id) {
        Task task = taskRepository.findOneById(id)
            .orElseThrow({ throw new NotFoundException(Task, [id: id]) })
        if (task.status != TaskStatus.SUCCESSFUL && task.status != TaskStatus.FAIL) {
            throw new IllegalArgumentException("Task($task) is not in final status")
        }

        taskRepository.delete(id)
    }

    Task cancelTask(Long id) {
        Task task = taskRepository.findOneById(id)
            .orElseThrow({ throw new NotFoundException(Task, [id: id]) })
        if (task.status != TaskStatus.WAIT && task.status != TaskStatus.IN_PROCESS) {
            throw new IllegalArgumentException("Task($task) in final status")
        }
        task.canceled = true
        taskService.changeStatus(task, TaskStatus.FAIL)
        taskRepository.save(task)
        taskService.currentTasks.get(task.id)?.cancel(true)
        taskService.currentTasks.remove(task.id)

        task
    }


    void startTask(Task task) {
        CompletableFuture<Task> future = taskService.processTask(task)
            .whenComplete({ Task result, Throwable throwable ->
            if (throwable) {
                taskService.fail(task, throwable)
            } else {
                taskService.successful(result)
            }
            taskService.currentTasks.remove(task.id)
        } as BiConsumer)
        taskService.currentTasks.putIfAbsent(task.id, future)
    }

    @Async('delegatingSecurityExecutor')
    CompletableFuture<Task> processTask(Task task) {
        taskRepository.findOneByIdAndCanceled(task.id, false)
        task.startedAt = LocalDateTime.now()
        taskService.changeStatus(task, TaskStatus.IN_PROCESS)
        taskRepository.save(task)
        log.info("Started task($task)")

        byte[] data = getByteData(task)
        String result
        switch (task.algorithm) {
            case Algorithm.MD5:
                result = DigestUtils.md5Hex(data)
                break
            case Algorithm.SHA1:
                result = DigestUtils.sha1Hex(data)
                break
            case Algorithm.SHA256:
                result = DigestUtils.sha256Hex(data)
                break
            default:
                throw new NoSuchAlgorithmException("Algorithm ${task.algorithm.name()} not found")
        }
        task.result = result
        CompletableFuture.completedFuture(task)
    }

    private static byte[] getByteData(Task task) throws Exception {
        boolean remoteFile = task.src.startsWith('http')
        if (remoteFile) {
            HttpClient httpClient = HttpClientBuilder.create().build()
            HttpGet get = new HttpGet(task.src)

            HttpResponse httpResponse = httpClient.execute(get)
            if (httpResponse.statusLine.statusCode == HttpStatus.OK.value()) {
                try {
                    return IOUtils.toByteArray(httpResponse.entity.content)
                } catch (Exception e) {
                    log.error("Error on downloading file from ${get.getURI()} response: ${EntityUtils.toString(httpResponse.entity)}", e)
                    throw e
                }
            }
        } else {
            return Files.readAllBytes(Paths.get(task.src))
        }
        return null
    }

    void successful(Task task) {
        task.finishedAt = LocalDateTime.now()
        taskService.changeStatus(task, TaskStatus.SUCCESSFUL)
        tasks.remove(task.id)
        taskRepository.save(task)
        log.info("Task($task) finished successful")
    }

    void fail(Task task, Throwable throwable) {
        task.with {
            finishedAt = LocalDateTime.now()
            error = getStackTraceAsString(throwable)
        }
        taskService.changeStatus(task, TaskStatus.FAIL)
        tasks.remove(task.id)
        taskRepository.save(task)
        log.error("Task($task) finished with exception", throwable)
    }

    Page<Task> findTaskByPage(Pageable pageable) {
        taskRepository.findAllByOwner(currentUserService.currentUser, pageable)
    }

    Task changeStatus(Task task, TaskStatus status) {
        task.status = status
        simpMessagingTemplate.convertAndSend("/topic/${task.owner.token.toString()}".toString(), task)
        task
    }

    private static String getStackTraceAsString(Throwable throwable) {
        new StringWriter().withCloseable {
            new PrintWriter(it).withCloseable {
                throwable.printStackTrace(new PrintWriter(it))
            }
            return it.toString()
        }
    }
}
