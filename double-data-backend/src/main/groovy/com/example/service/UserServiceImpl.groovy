package com.example.service

import com.example.model.User
import com.example.repository.UserRepository
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@CompileStatic
@Service
class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository

    User login(String token) {
        User user = new User()
        if (token) {
            user.token = UUID.fromString(token)
        }
        userRepository.save(user)
    }

    Optional<User> findByToken(String token) {
        userRepository.findOneByToken(UUID.fromString(token))
    }
}
