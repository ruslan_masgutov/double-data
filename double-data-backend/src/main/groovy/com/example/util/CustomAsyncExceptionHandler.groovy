package com.example.util

import com.example.model.Task
import com.example.service.TaskService
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.lang.reflect.Method

@Slf4j
@CompileStatic
@Component
class CustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
    @Autowired
    TaskService taskService

    @Override
    void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
        log.error("Exception message: $throwable.message \n Method name: $method.name", throwable)
        taskService.fail(obj[0] as Task, throwable)
        for (Object param : obj) {
            log.error("Parameter value: $param")
        }
    }
}