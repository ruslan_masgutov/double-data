package com.example.util

import com.example.model.Task
import com.example.model.TaskStatus
import com.example.repository.TaskRepository
import com.example.service.TaskService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@CompileStatic
@Profile('!test')
@Component
class ApplicationReadyEventHook implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    TaskService taskService
    @Autowired
    TaskRepository taskRepository

    @Override
    void onApplicationEvent(ApplicationReadyEvent event) {
        taskRepository.findAllByStatusInAndCanceled([TaskStatus.IN_PROCESS, TaskStatus.WAIT], false)
            .forEach({ Task task ->
            taskService.startTask(task)
        })
    }
}
