package com.example.util

import com.example.DoubleDataBackendApplication
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.hibernate.engine.spi.SessionImplementor
import org.hibernate.id.enhanced.SequenceStyleGenerator
import org.springframework.jdbc.core.JdbcTemplate

import javax.persistence.Table

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Slf4j
class SequenceGeneratorApp extends SequenceStyleGenerator {

    private JdbcTemplate jdbcTemplate

    private static final String SEQUENCE_STATEMENT = "select nextval ('%s_id_seq'::regclass)";

    @Override
    Serializable generate(SessionImplementor session, Object obj) {
        if (!jdbcTemplate) {
            jdbcTemplate = DoubleDataBackendApplication.applicationContext.getBean(JdbcTemplate)
        }
        if (obj.getClass().isAnnotationPresent(Table)) {
            String tableName = obj.getClass().getAnnotation(Table).name()
            return jdbcTemplate.queryForObject(String.format(SEQUENCE_STATEMENT, tableName), Long)
        } else {
            log.info("Not found sequence for %s", obj.getClass().getName())
            return super.generate(session, obj)
        }
    }
}
