package com.example.util

import groovy.transform.CompileStatic

import javax.persistence.AttributeConverter
import javax.persistence.Converter
import java.time.LocalDateTime
import java.time.ZoneId

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Converter(autoApply = true)
class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Date> {
    @Override
    Date convertToDatabaseColumn(LocalDateTime attribute) {
        attribute ? Date.from(attribute.atZone(ZoneId.systemDefault()).toInstant()) : null
    }

    @Override
    LocalDateTime convertToEntityAttribute(Date dbData) {
        dbData ? LocalDateTime.ofInstant(dbData.toInstant(), ZoneId.systemDefault()) : null
    }
}
