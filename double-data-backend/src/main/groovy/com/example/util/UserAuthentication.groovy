package com.example.util

import com.example.model.User
import groovy.transform.CompileStatic
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority

@CompileStatic
class UserAuthentication implements Authentication {

    private final User user
    private boolean authenticated = true

    UserAuthentication(User user) {
        this.user = user
    }

    @Override
    String getName() {
        return user.token
    }

    @Override
    Collection<? extends GrantedAuthority> getAuthorities() {
        null
    }

    @Override
    Object getCredentials() {
        null
    }

    @Override
    User getDetails() {
        user
    }

    @Override
    Object getPrincipal() {
        user.token
    }

    @Override
    boolean isAuthenticated() {
        authenticated
    }

    @Override
    void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated
    }
}