package com.example.util

import com.example.model.User
import com.example.service.CurrentUserService
import com.example.service.UserService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Component
class CurrentUserFilter extends GenericFilterBean {
    private static final String AUTH_HEADER_NAME = "Authorization"
    @Autowired
    private UserService userService
    @Autowired
    private CurrentUserService currentUserService

    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = request as HttpServletRequest
        String token = httpServletRequest.getHeader(AUTH_HEADER_NAME)
        if (!token) {
            token = request.getParameter(AUTH_HEADER_NAME)
        }
        User user
        if (token) {
            user = userService.findByToken(token)
                .orElse(userService.login(token))
        } else {
            user = userService.login(null)
        }
        currentUserService.currentUser = user
        (response as HttpServletResponse).addHeader(AUTH_HEADER_NAME, user.token.toString())
        chain.doFilter(request, response)
        currentUserService.currentUser = null
    }
}
