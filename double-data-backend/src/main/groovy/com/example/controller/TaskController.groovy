package com.example.controller

import com.example.model.Algorithm
import com.example.model.Task
import com.example.service.TaskService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.*


@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@RestController
@RequestMapping(value = '/api/tasks')
class TaskController {
    @Autowired
    private TaskService taskService

    @GetMapping
    Page<Task> getPage(@PageableDefault Pageable pageable) {
        PageRequest request = new PageRequest(pageable.pageNumber, pageable.pageSize, Sort.Direction.ASC, 'id')
        taskService.findTaskByPage(request)
    }

    @PostMapping
    Task createTask(@RequestParam String src, @RequestParam Algorithm algo) {
        taskService.createTask(src, algo)
    }

    @PutMapping(path = '/{id}/cancel')
    Task cancelTask(@PathVariable Long id) {
        taskService.cancelTask(id)
    }

    @DeleteMapping(path = '/{id}')
    void deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id)
    }

}
