package com.example.controller

import com.example.model.User
import com.example.service.CurrentUserService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@RestController
@RequestMapping(value = '/api/users')
class UserController {
    @Autowired
    private CurrentUserService currentUserService

    @GetMapping
    User getCurrentUser() {
        currentUserService.currentUser
    }
}
