package com.example

import groovy.transform.CompileStatic
import org.springframework.beans.BeansException
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

@CompileStatic
@SpringBootApplication
class DoubleDataBackendApplication implements ApplicationContextAware {

    static ApplicationContext applicationContext

    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        DoubleDataBackendApplication.applicationContext = applicationContext
    }

    static void main(String[] args) {
        SpringApplication.run(DoubleDataBackendApplication, args)
    }
}
