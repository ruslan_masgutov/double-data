package com.example.repository

import com.example.model.Task
import com.example.model.TaskStatus
import com.example.model.User
import groovy.transform.CompileStatic
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

import java.util.stream.Stream

@CompileStatic
interface TaskRepository extends JpaRepository<Task, Long> {

    Page<Task> findAllByOwner(User owner, Pageable pageable)

    Optional<Task> findOneById(Long id)

    Stream<Task> findAllByStatusInAndCanceled(List<TaskStatus> statuses, Boolean canceled)

    Optional<Task> findOneByIdAndCanceled(Long id, Boolean canceled)
}
