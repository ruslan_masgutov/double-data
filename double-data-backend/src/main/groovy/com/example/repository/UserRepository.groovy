package com.example.repository

import com.example.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByToken(UUID token)
}