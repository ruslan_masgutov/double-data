package com.example.exception

class NotFoundException extends RuntimeException {
    NotFoundException() {
    }

    NotFoundException(String message) {
        super(message)
    }

    NotFoundException(String message, Throwable cause) {
        super(message, cause)
    }

    NotFoundException(Class aClass, Map params) {
        super("${aClass.simpleName} not found (${params.toMapString()})")
    }
}
