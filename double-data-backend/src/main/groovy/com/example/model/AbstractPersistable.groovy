package com.example.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.GenericGenerator
import org.springframework.data.domain.Persistable

import javax.persistence.*

@CompileStatic
@MappedSuperclass
@ToString
@EqualsAndHashCode(includes = ['id'])
class AbstractPersistable implements Persistable<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceGeneratorApp")
    @GenericGenerator(name = "SequenceGeneratorApp", strategy = "com.example.util.SequenceGeneratorApp")
    Long id

    Long getId() {
        id
    }

    @Transient
    @JsonIgnore
    boolean isNew() {
        null == getId()
    }
}
