package com.example.model

import groovy.transform.CompileStatic

@CompileStatic
enum Algorithm {
    MD5, SHA1, SHA256
}