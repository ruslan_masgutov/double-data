package com.example.model

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*
import java.time.LocalDateTime

@CompileStatic
@ToString
@EqualsAndHashCode(excludes = ['owner', 'status', 'createdAt'])
@Entity
@Table(name = 'tasks')
class Task extends AbstractPersistable {
    String src

    @Enumerated(EnumType.STRING)
    Algorithm algorithm

    @Enumerated(EnumType.STRING)
    TaskStatus status = TaskStatus.WAIT

    String result

    String error

    LocalDateTime createdAt = LocalDateTime.now()

    @ManyToOne
    User owner

    LocalDateTime startedAt
    LocalDateTime finishedAt

    Boolean canceled = false
}
