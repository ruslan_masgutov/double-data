package com.example.model

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.Table
import java.time.LocalDateTime

@CompileStatic
@ToString
@EqualsAndHashCode(includes = ['token'])
@Entity
@Table(name = 'users')
class User extends AbstractPersistable {
    UUID token = UUID.randomUUID()

    LocalDateTime createdDate = LocalDateTime.now()
}
