package com.example.model

import groovy.transform.CompileStatic

@CompileStatic
enum TaskStatus {
    WAIT, IN_PROCESS, SUCCESSFUL, FAIL
}