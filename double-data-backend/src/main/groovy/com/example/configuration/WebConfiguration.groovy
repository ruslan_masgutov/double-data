package com.example.configuration

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import groovy.transform.CompileStatic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Configuration
class WebConfiguration {
    @Bean
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(ObjectMapper mapper) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter()
        mapper.enable(MapperFeature.DEFAULT_VIEW_INCLUSION)
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
        mapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE)
        mapper.setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.ANY)
        Hibernate5Module hibernate5Module = new Hibernate5Module()
        hibernate5Module.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION)
        mapper.registerModule(hibernate5Module)
        JavaTimeModule javaTimeModule = new JavaTimeModule()
        mapper.registerModule(javaTimeModule)
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
        mapper.serializationInclusion = JsonInclude.Include.NON_NULL
        converter.objectMapper = mapper
        return converter
    }
}
