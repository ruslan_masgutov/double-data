package com.example.configuration

import com.example.util.CustomAsyncExceptionHandler
import groovy.transform.CompileStatic
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.AsyncConfigurerSupport
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor

import java.util.concurrent.Executor

@SuppressWarnings("GroovyUnusedDeclaration")
@CompileStatic
@Configuration
@EnableAsync
class AsyncConfiguration extends AsyncConfigurerSupport {
    @Override
    @Bean(name = 'delegatingSecurityExecutor')
    Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor(corePoolSize: 2, maxPoolSize: 5, queueCapacity: 500, threadNamePrefix: 'double-data-')
        executor.initialize()
        executor
        new DelegatingSecurityContextExecutor(executor)
    }

    @Override
    AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        new CustomAsyncExceptionHandler()
    }

}
