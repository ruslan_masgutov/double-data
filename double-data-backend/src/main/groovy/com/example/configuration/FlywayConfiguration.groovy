package com.example.configuration

import org.flywaydb.core.Flyway
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

import javax.sql.DataSource

@SuppressWarnings(["SpringJavaAutowiringInspection", "GroovyUnusedDeclaration"])
@Configuration
class FlywayConfiguration {
    @Bean(name = "flyway")
    @Profile("!test")
    Flyway flyway(DataSource dataSource) {
        Flyway flyway = new Flyway(dataSource: dataSource, locations: ["classpath:/db/auto"],
            sqlMigrationPrefix: 'V', validateOnMigrate: false, outOfOrder: true)
        flyway.migrate()
        flyway
    }

    @Bean(name = "flyway")
    @Profile("test")
    Flyway flywayTest(DataSource dataSource) {
        Flyway flyway = new Flyway(dataSource: dataSource, locations: ["classpath:/db/auto", "classpath:/db/test"],
            sqlMigrationPrefix: 'V', validateOnMigrate: false, outOfOrder: true)
        flyway.clean()
        flyway.migrate()
        flyway
    }
}
